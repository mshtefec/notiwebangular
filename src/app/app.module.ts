import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FrontModule } from './front/front.module';
import { AdminModule } from './back/admin/admin.module';
import { PagenotfoundComponent } from './util/pagenotfound/pagenotfound.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 5000,
    }),
    FrontModule,
    AdminModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    PagenotfoundComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
