import { Component, OnInit } from '@angular/core';

//SERVICES
import { NoticeService }      from '../../services/notice.service';
//MODEL
import { Notice }             from '../../models/notice'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  list: Notice[];

  constructor(
    private noticeService: NoticeService
  ) { }

  ngOnInit() {
    this.noticeService.getNoticesActives()
      .snapshotChanges()
      .subscribe(item => {
        this.list = [];
        item.forEach(element => {
          let x = element.payload.toJSON();
          x['$key'] = element.key;
          this.list.push(x as Notice);
        });
      });
  }

}
