import { Injectable }                           from '@angular/core';

//FIREBASE
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AngularFireAuth }                      from 'angularfire2/auth';

//MODEL
import { Notice }                               from '../models/notice';

@Injectable({
  providedIn: 'root'
})
export class NoticeService {

  list: AngularFireList<any>;
  selectedNotice: Notice = new Notice();

  constructor(
    private firebase: AngularFireDatabase,
    //public angularFireAuth: AngularFireAuth
  ) { }

  getNotices(){
    return this.list = this.firebase.list('notices');
  }

  getNoticesActives(){
    return this.list = this.firebase.list('notices', ref => ref.orderByChild('active').equalTo(true));
  }

  insert(notice: Notice){
    const myObjStr = JSON.stringify(notice);
    // this.list.push({
    //   title:            notice.title,
    //   shortDescription: notice.shortDescription,
    //   //description: notice.description,
    //   urlToImg:         notice.urlToImg,
    //   category:         notice.category,
    //   hashTag:          notice.hashTag,
    //   active:           notice.active,
    //   isNews:           notice.isNews
    // });
    const obj = JSON.parse(myObjStr);
    const itemRef = this.firebase.object('item');
    itemRef.set({ obj });

    this.list.push({
      obj
    });
  }

  update(notice: Notice){
    this.list.update(notice.$key, {
      title:            notice.title,
      shortDescription: notice.shortDescription,
      //description: notice.description,
      urlToImg:         notice.urlToImg,
      category:         notice.category,
      hashTag:          notice.hashTag,
      active:           notice.active,
      isNews:           notice.isNews
    })
  }

  deleted($key: string){
    this.list.remove($key);
  }
}
