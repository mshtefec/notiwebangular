import { Injectable } from '@angular/core';

import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  list: AngularFireList<any>;
  selectedUser: User = new User();

  constructor(
    private firebase: AngularFireDatabase,
    //public angularFireAuth: AngularFireAuth
  ) { }

  getUsers(){
    return this.list = this.firebase.list('users');
  }

  insert(user: User){
    this.list.push({
      name:     user.name,
      username: user.username,
      email:    user.email,
      password: user.password,
      active:   user.active
    })

    // return new Promise((resolve, reject) => {
    //   this.angularFireAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
    //     .then( userData =>  resolve(userData),
    //     err => reject (err)); 
    // });
  }

  update(user: User){
    this.list.update(user.$key, {
      name:     user.name,
      username: user.username,
      email:    user.email,
      password: user.password,
      active:   user.active
    })

  }

  delete($key: string){
    this.list.remove($key);
  }

}