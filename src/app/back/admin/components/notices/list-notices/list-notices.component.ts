import { Component, OnInit }  from '@angular/core';

//SERVICES
import { NoticeService }      from '../../../../../services/notice.service';
//MODEL
import { Notice }             from '../../../../../models/notice'

//UTILS
//...
@Component({
  selector:     'app-list-notices',
  templateUrl:  './list-notices.component.html',
  styleUrls:    ['./list-notices.component.css']
})
export class ListNoticesComponent implements OnInit {

  list: Notice[];

  constructor(
    private noticeService: NoticeService
  ) { }

  ngOnInit() {
    this.noticeService.getNotices()
      .snapshotChanges()
      .subscribe(item => {
        this.list = [];
        item.forEach(element => {
          let x = element.payload.toJSON();
          x['$key'] = element.key;
          this.list.push(x as Notice);
        });
      });
  }

  onEdit(notice: Notice) {
    // la linea comentada lo que hace es tener un enlace directamente con el objeto
    // con esto se puede editar en tiempo real pero,
    // en grandes usuarios puede tener problemas de rendimiento por eso se realiza 
    // una copia del objeto con el metodo y se deben agregar las funciones de control
    // this.userService.selectedUser = user;
    this.noticeService.selectedNotice = Object.assign({}, notice);
  }

  onDelete($key: string) {
    if(confirm('Are you sure you want to delete it?')) {
      this.noticeService.deleted($key);
      //this.toastrService.success('Successfull Operation', 'Product Deleted');
    }
  }

}
