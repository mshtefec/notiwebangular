import { Injectable }                               from '@angular/core';
import { AngularFireDatabase, AngularFireList }     from 'angularfire2/database';
import { Section }                                  from '../models/section';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

    list: AngularFireList<any>;
    selectedSection: Section = new Section();

    constructor(
        private firebase: AngularFireDatabase,
    ) { }
    
    getAll(){
        return this.list = this.firebase.list('sections');
    }
}