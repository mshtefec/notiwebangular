export class User {
    $key:       string;
    id:         string;
    name:       string;
    username:   string;
    email:      string;
    password:   string;
    active:     boolean;

    constructor(name?: string, username?: string, email?: string, password?: string, active: boolean = false) {
        this.name       = name;
        this.username   = username;
        this.email      = email;
        this.password   = password;
        this.active     = active;
    }
}
