import { NgModule }           from '@angular/core';
import { CommonModule }       from '@angular/common';

import { HomeComponent }      from '../front/home/home.component'

import { FrontRoutingModule } from './front-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FrontRoutingModule
  ],
  declarations: [
    HomeComponent
  ]
})
export class FrontModule { }