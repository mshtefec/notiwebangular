import { Component, OnInit }  from '@angular/core';
//SERVICES
import { UserService }      from '../../../../../services/user.service';
//MODEL
import { User }             from '../../../../../models/user'

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  list: User[];

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.userService.getUsers()
      .snapshotChanges()
      .subscribe(item => {
        this.list = [];
        item.forEach(element => {
          let x = element.payload.toJSON();
          x['$key'] = element.key;
          this.list.push(x as User);
        });
      });
  }

  onEdit(user: User) {
    // la linea comentada lo que hace es tener un enlace directamente con el objeto
    // con esto se puede editar en tiempo real pero,
    // en grandes usuarios puede tener problemas de rendimiento por eso se realiza 
    // una copia del objeto con el metodo y se deben agregar las funciones de control
    // this.userService.selectedUser = user;
    this.userService.selectedUser = Object.assign({}, user);
  }

  onDelete($key: string) {
    if(confirm('Are you sure you want to delete it?')) {
      this.userService.delete($key);
      //this.toastrService.success('Successfull Operation', 'Product Deleted');
    }
  }

}
