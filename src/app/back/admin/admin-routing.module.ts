import { NgModule }               from '@angular/core';
import { RouterModule, Routes }   from '@angular/router';
/* api login */
import { LoginComponent }         from '../../api/login/login.component';
import { RegisterComponent }      from '../../api/register/register.component';
import { AuthGuard }              from '../../guards/auth.guard';
//ADMIN
import { AdminComponent }         from './admin.component';
//NOTICIAS
import { ListNoticesComponent }   from './components/notices/list-notices/list-notices.component';
import { FormNoticeComponent }    from './components/notices/form-notice/form-notice.component';
//USUARIOS
import { ListUsersComponent }   from './components/users/list-users/list-users.component';
import { FormUserComponent }    from './components/users/form-user/form-user.component';
import { SectionComponent } from '../../backend/components/section.component';
//SECCIONES


const routes: Routes = [
    { 
      path: 'admin', 
      component: AdminComponent,
      canActivate: [AuthGuard],
      children: [
        {
          path: '',
          children: [
            { path: 'notices', component: ListNoticesComponent },
            { path: 'notices/new', component: FormNoticeComponent },
            { path: 'notices/:id', component: FormNoticeComponent },
            { path: 'users', component: ListUsersComponent },
            { path: 'users/new', component: FormUserComponent },
            { path: 'users/:id', component: FormUserComponent },
            { path: 'sections', component: SectionComponent },
          ]
        },
        { path: '', redirectTo: 'notices', pathMatch: 'full' },
      ]
    },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: '**', redirectTo: 'admin/notices', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule { }
