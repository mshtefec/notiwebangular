import { NgModule }                     from '@angular/core';
import { CommonModule }                 from '@angular/common';
import { FormsModule }                  from '@angular/forms';

//COMPONENTES
/* menu */
import { MenuComponent }                from '../menu/menu.component';
/* admin */
import { AdminComponent }               from './admin.component'
/* secciones */
import { SectionComponent }             from '../../backend/components/section.component';
/* noticias */
import { ListNoticesComponent }         from './components/notices/list-notices/list-notices.component'
import { FormNoticeComponent }          from './components/notices/form-notice/form-notice.component';
/* usuarios */
import { ListUsersComponent }           from './components/users/list-users/list-users.component'
import { FormUserComponent }            from './components/users/form-user/form-user.component';
/* api login */
import { LoginComponent }               from '../../api/login/login.component';
import { RegisterComponent }            from '../../api/register/register.component';
//RUTAS
import { AdminRoutingModule }           from './admin-routing.module'; 

//LIBRERIAS
import { CKEditorModule }               from '@ckeditor/ckeditor5-angular';
/* firebase */
import { AngularFireModule }            from 'angularfire2';
import { AngularFireAuthModule }        from 'angularfire2/auth';
import { AngularFireDatabaseModule }    from 'angularfire2/database';
/* configs */
import { environment }                  from '../../../environments/environment';
//Services
import { UserService }                  from '../../services/user.service';
import { AuthService }                  from '../../services/auth.service';
import { AuthGuard }                    from '../../guards/auth.guard';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CKEditorModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AdminRoutingModule
  ],
  declarations: [
    MenuComponent,
    LoginComponent,
    RegisterComponent,    
    //ADMIN
    AdminComponent,
    //SECCIONES
    SectionComponent,
    //NOTICES
    ListNoticesComponent,
    FormNoticeComponent,
    //USERS
    ListUsersComponent,
    FormUserComponent
  ],
  providers: [
    UserService,
    AuthService,
    AuthGuard
  ]
})
export class AdminModule { }
