import { Component, OnInit }    from '@angular/core';
import { NgForm }               from '@angular/forms';
import { Router }               from '@angular/router';
//SERVICES
import { UserService }        from '../../../../../services/User.service';
//MODEL
import { User }               from '../../../../../models/User'
//UTILS
import * as ClassicEditor       from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'app-form-user',
  templateUrl: './form-user.component.html',
  styleUrls: ['./form-user.component.css']
})
export class FormUserComponent implements OnInit {

  public Editor = ClassicEditor;
  
  constructor(
    public router: Router,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.userService.getUsers();
    this.resetForm();
  }

  onSubmit(userForm: NgForm) {
    if(userForm.value.$key == null) {
      this.userService.insert(userForm.value);
    } else {
      this.userService.update(userForm.value);
    }    
    this.resetForm(userForm);
    this.router.navigate(['admin/users']);
  }

  resetForm(userForm?: NgForm) {
    if(userForm != null){
      userForm.reset();
      this.userService.selectedUser = new User();
    }
  }

}
