import { Component, OnInit }    from '@angular/core';
import { NgForm }               from '@angular/forms';
import { Router }               from '@angular/router';
//SERVICES
import { NoticeService }        from '../../../../../services/notice.service';
//MODEL
import { Notice }               from '../../../../../models/notice'
//UTILS
import * as ClassicEditor       from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector:     'app-form-notice',
  templateUrl:  './form-notice.component.html',
  styleUrls:    ['./form-notice.component.css']
})
export class FormNoticeComponent implements OnInit {

  public Editor = ClassicEditor;
  
  constructor(
    public router: Router,
    private noticeService: NoticeService
  ) { }

  ngOnInit() {
    this.noticeService.getNotices();
    this.resetForm();
  }

  onSubmit(noticeForm: NgForm) {
    if(noticeForm.value.$key == null) {
      this.noticeService.insert(noticeForm.value);
    } else {
      this.noticeService.update(noticeForm.value);
    }    
    this.resetForm(noticeForm);
    this.router.navigate(['admin/notices']);
  }

  resetForm(noticeForm?: NgForm) {
    if(noticeForm != null){
      noticeForm.reset();
      this.noticeService.selectedNotice = new Notice();
    }
  }

}
