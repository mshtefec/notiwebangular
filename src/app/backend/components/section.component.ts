import { Component } from '@angular/core';
import { UtilsService } from '../../services/utils.service';
import { Section } from '../../models/section'; 

@Component({
    selector: 'app-section',
    styleUrls: ['./backend.css'],
    template: `
        <div class="ui container">
            <div class="ui card">
                <div class="content">
                    <div class="right floated meta"><i class="chevron down icon"></i></div>
                    <div class="header"><i class="list icon"></i>{{ header_title }}</div>
                </div>
                <div class="content">
                    <table class="ui selectable celled very compact table">
                        <thead>
                            <tr>
                                <th *ngFor="let column of columns">{{ column }}</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr *ngFor="let element of list">
                                <td>{{ element.id }}</td>
                                <td>{{ element.name }}</td>
                                <td>{{ element.detail }}</td>
                                <td class="collapsing">
                                    <i *ngIf="element.active" class="check circle icon blue"></i>
                                    <i *ngIf="!element.active" class="times circle icon red"></i>
                                </td>
                                <td>
                                    <button class="mini ui button icon" (click)="onDelete(element.$key)"><i class="times icon"></i></button>
                                    <button class="mini ui button icon" routerLink="{{ element.$key }}" (click)="onEdit(element)"><i class="edit icon"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="extra content">
                    <a class="ui primary button" routerLink="new"><i class="plus icon"></i> Nueva {{ title }}</a>
                </div>
            </div>
        </div>
    `
})
export class SectionComponent {
    title = 'Seccion';
    header_title = 'Listado de Secciones';

    columns = ['Id', 'Nombre', 'Detalle', 'Activo'];     
    //list: Section[];
    list = ELEMENT_DATA;

    constructor(
        private utilsService: UtilsService
    ) { }

    ngOnInit() {
        // this.utilsService.getAll()
        // .snapshotChanges()
        // .subscribe(item => {
        //     this.list = [];
        //     item.forEach(element => {
        //     let x = element.payload.toJSON();
        //     x['$key'] = element.key;
        //     this.list.push(x as Section);
        //     });
        // });
    }
}

export interface ExampleElement {
    id:         number;
    name:       string;
    detail:     string;
    active:     boolean;
}

const ELEMENT_DATA: ExampleElement[] = [
    { id: 1, name: 'nuevas', detail: 'this is a section', active: true },
    { id: 2, name: 'entretenimiento', detail: 'this is a section', active: false },
    { id: 3, name: 'nacionales', detail: 'this is a section', active: true },
    { id: 4, name: 'tecnologia', detail: 'this is a section', active: false },
    { id: 5, name: 'musica', detail: 'this is a section', active: true },
];