import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { ToastrService } from 'ngx-toastr';

import { UserService } from '../../services/user.service'
import { User } from '../../models/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public email: string;
  public password: string;
  constructor(
    private authService: AuthService,
    private router: Router,
    private toastrService: ToastrService,
    public userService: UserService
  ) { }

  ngOnInit() {
  }

  onSubmitAddUser() {
    this.authService.registerUser(this.email, this.password)
    .then((res) => {

      let user = new User('name','username',this.email,this.password);
      this.userService.insert(user);

      this.toastrService.success('Exito!', 'El usuario ha sido registrado');
      this.router.navigate(['admin/home']);
    }).catch( (err) => {
      this.toastrService.error('Error', 'No pudo completarse el registro');
    });
  }

}