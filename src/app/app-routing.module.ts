import { NgModule } from '@angular/core';
import { RouterModule, Routes  } from '@angular/router';

import { PagenotfoundComponent } from './util/pagenotfound/pagenotfound.component'

const routes: Routes = [
  { path: 'admin', redirectTo: '/admin', pathMatch: 'full' },
  { path: '', redirectTo: '/admin', pathMatch: 'full' },
  //{ path: '**', component: PagenotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
