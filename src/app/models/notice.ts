export class Notice {
    $key:               string;
    id:                 string;
    date:               string;
    title:              string;
    shortDescription:   string;
    //description: string;
    urlToImg:           string;
    category:           string;
    hashTag:            string;
    active:             boolean;
    isNews:             boolean;

    constructor(title?: string, shortDescription?: string, urlToImg?: string, category?: string, hashTag?: string, active: boolean = false, isNews: boolean = false) {
        this.title              = title,
        this.shortDescription   = shortDescription,
        //this.description = description,
        this.urlToImg           = urlToImg,
        this.category           = category,
        this.hashTag            = hashTag,
        this.active             = active,
        this.isNews             = isNews
    }
}
