export class Section {
    $key:       string;
    id:         string;
    name:       string;
    detail:     string;
    active:     boolean;

    constructor(name?: string, detail?: string, active: boolean = false) {
        this.name       = name;
        this.detail     = detail;
        this.active     = active;
    }
}