import { Injectable }       from '@angular/core';
import { AngularFireAuth }  from 'angularfire2/auth';
import { map }              from 'rxjs/operators'

//import { UserService }      from '../services/user.service'
//import { User }             from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    public afAuth: AngularFireAuth,
    //public userService: UserService
  ) { }

  registerUser(email: string, pass: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.createUserWithEmailAndPassword(email, pass)
      .then( userData =>  resolve(userData),
      err => reject (err));

      // if (resolve) {
      //   let user = new User('name','username', email, pass);
      //   this.userService.insertUser(user);
      // }
    });
  }

  loginEmail(email: string, pass: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(email, pass)
      .then( userData =>  resolve(userData),
      err => reject (err));
    });
  }

  getAuth() {
    return this.afAuth.authState.pipe(map(auth => auth));
  }

  logout() {
    return this.afAuth.auth.signOut();
  }
}
