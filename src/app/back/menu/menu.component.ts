import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  public isLogin: boolean;
  public nombreUsuario: string;

  constructor(
    public authService: AuthService,
    public router: Router,
    private toastrService: ToastrService
  ) { }

  ngOnInit() {
    this.authService.getAuth().subscribe( auth => {
      if (auth) {
        this.isLogin = true;
        this.nombreUsuario = auth.displayName;
      } else {
        this.isLogin = false;
      }
    });
  }

  onClickLogout() {
    this.toastrService.success('Exito', 'Hasta luego!!');
    this.authService.logout();
    this.router.navigate(['home']);
  }

}
